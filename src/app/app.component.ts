import { Component, SimpleChanges } from '@angular/core';
import { Commonservice } from './services/commonservice';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'skyzone';
  public headerFooterSectionShow: Boolean = true;

  constructor(public commonService: Commonservice, private router: Router) {
    router.events.subscribe((val) => {
      // see also 
      let pathName = window.location.pathname;
      pathName = pathName.substr(1);
      if (this.commonService.headerFooterHidePages.indexOf(pathName) > -1) {
        this.headerFooterSectionShow = false;
      } else {
        this.headerFooterSectionShow = true;
      }
    });
  }

}
