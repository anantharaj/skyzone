import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})

export class DefaultLayoutComponent {
  // menu modeltype droplist
  showmt: boolean = false;
  constructor(
    private loadingBar: LoadingBarService,
    @Inject(DOCUMENT) _document?: any
  ) { }

  ngOnInit() {
  }

  logout() {
    this.loadingBar.start();
    
    this.loadingBar.complete();
  }

  public mtMenuClick(): void {
    this.showmt = !this.showmt;
  }

}
