import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhyskyzoneComponent } from './whyskyzone.component';

describe('WhyskyzoneComponent', () => {
  let component: WhyskyzoneComponent;
  let fixture: ComponentFixture<WhyskyzoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhyskyzoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhyskyzoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
