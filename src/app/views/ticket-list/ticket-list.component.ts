import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {

  constructor(public router: Router) {

  }

  ngOnInit() {
  }

  public ticketDetailPage(index) {
    localStorage.setItem('clickedTicket', index);
    this.router.navigate(['/ticketdetail']);
  }

}
