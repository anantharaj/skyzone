import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss']
})
export class TicketDetailComponent implements OnInit {
  public qty: number;
  constructor(public router: Router) {
    this.qty = 1;
  }

  ngOnInit() {
  }

  qtyFunc(operator) {
    this.qty = (operator === '-') ? (this.qty - 1) : (this.qty + 1);
    if (this.qty <= 1) {
      this.qty = 1;
    }
  }

  buyTicket(){
    const localStorageData = JSON.parse(localStorage.getItem('checkOutData'));

    /** Get Selected Product detail id */
    const clickedTicket = localStorage.getItem('clickedTicket');
    const getTicket = (localStorageData.filter(tab => Number(tab.id) === Number(clickedTicket)))[0];

    /** Update qty & Total based on Enter Qty */
    getTicket.qty = this.qty + getTicket.qty;
    getTicket.total = (getTicket.price * getTicket.qty).toFixed(2);
    const index = (localStorageData.findIndex(tab => Number(tab.id) === Number(clickedTicket)));

    /** Overwrite value in local storage */
    localStorageData[index] = getTicket;

    /** Code to set cart count */
    const countTicket = (localStorageData.filter(tab => Number(tab.qty) > 0));

    /** Set to localStorage */
    localStorage.setItem('checkOutData', JSON.stringify(localStorageData));
    localStorage.setItem('cartCount', countTicket.length);
    this.router.navigate(['/cart']);
  }

}
