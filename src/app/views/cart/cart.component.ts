import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Commonservice } from '../../services/commonservice';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  @ViewChild('myModal') public myModal: ModalDirective;
  public productDataList = [];
  public selectedProductId: any;
  public subTotal = 0;
  constructor(public commonService: Commonservice, public router: Router) {
    this.getProductList();
  }

  ngOnInit() {
  }

  getProductList() {
    const dataList = JSON.parse(localStorage.getItem('checkOutData'));
    this.productDataList = dataList.filter(function (obj) {
      return obj.qty !== 0;
    });

    this.subTotal = 0;
    for(let i = 0; i<this.productDataList.length; i++){
      this.subTotal += (this.productDataList[i].price * this.productDataList[i].qty);
    }
  }

  deleteProduct(productId) {
    this.myModal.hide();
    const localStorageData = JSON.parse(localStorage.getItem('checkOutData'));

    /** Get Selected Product detail id */
    const getTicket = (localStorageData.filter(tab => Number(tab.id) === Number(productId)))[0];

    /** Update qty & Total based on Enter Qty */
    getTicket.qty = 0;
    getTicket.total = 0;
    const index = (localStorageData.findIndex(tab => Number(tab.id) === Number(productId)));

    /** Overwrite value in local storage */
    localStorageData[index] = getTicket;

    /** Code to set cart count */
    const countTicket = (localStorageData.filter(tab => Number(tab.qty) > 0));

    /** Set to localStorage */
    localStorage.setItem('checkOutData', JSON.stringify(localStorageData));
    localStorage.setItem('cartCount', countTicket.length);

    this.getProductList();
  }

  checkoutPage(){
    this.router.navigate(['/checkout']);
  }

}
