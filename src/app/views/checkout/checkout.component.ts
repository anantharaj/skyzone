import { Component, OnInit } from '@angular/core';
import { Commonservice } from '../../services/commonservice';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  public productDataList = [];
  public subTotal = 0;
  constructor(public commonService: Commonservice, public router: Router) {
    this.getProductList();
  }

  ngOnInit() {
  }

  getProductList() {
    const dataList = JSON.parse(localStorage.getItem('checkOutData'));
    this.productDataList = dataList.filter(function (obj) {
      return obj.qty !== 0;
    });

    this.subTotal = 0;
    for (let i = 0; i < this.productDataList.length; i++) {
      this.subTotal += (this.productDataList[i].price * this.productDataList[i].qty);
    }
  }
}
