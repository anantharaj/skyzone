import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() {
    localStorage.removeItem('clickedTicket');
    localStorage.removeItem('checkOutData');
    localStorage.removeItem('cartCount');

    const localStorageData = [
      {
        id: 1,
        name: '30 Minutes Ticket',
        price: 50,
        qty: 0,
        total: 0,
        image: '../../assets/images/header1.jpg'
      }, {
        id: 2,
        name: '60 Minutes Ticket',
        price: 80,
        qty: 0,
        total: 0,
        image: '../../assets/images/header1.jpg'
      },
      {
        id: 3,
        name: '90 Minutes Ticket',
        price: 100,
        qty: 0,
        total: 0,
        image: '../../assets/images/header1.jpg'
      },
      {
        id: 4,
        name: '120 Minutes Ticket',
        price: 110,
        qty: 0,
        total: 0,
        image: '../../assets/images/header1.jpg'
      }];

    /** Set to localStorage */
    localStorage.setItem('checkOutData', JSON.stringify(localStorageData));
  }

  ngOnInit() {
  }

}
