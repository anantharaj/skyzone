import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Commonservice {
  public taxAmount = 2.50;
  public headerFooterHidePages = ['payment'];
  constructor() { }
}
