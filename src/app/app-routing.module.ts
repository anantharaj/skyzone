import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './layout';
import { P404Component } from './p404/p404.component';
import { CallbackComponent } from './callback/callback.component';
import { P500Component } from './p500/p500.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { TicketListComponent } from './views/ticket-list/ticket-list.component';
import { TicketDetailComponent } from './views/ticket-detail/ticket-detail.component';
import { CartComponent } from './views/cart/cart.component';
import { CheckoutComponent } from './views/checkout/checkout.component';
import { PaymentComponent } from './views/payment/payment.component';
import { ContactusComponent } from './views/contactus/contactus.component';
import { SafetyComponent } from './views/safety/safety.component';
import { WhyskyzoneComponent } from './views/whyskyzone/whyskyzone.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '404', component: P404Component, data: { title: 'Page 404' } },
  { path: '500', component: P500Component, data: { title: 'Page 500' } },
  { path: 'callback', component: CallbackComponent, data: { title: 'Register Page' } },
  { path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard Page' } },
  { path: 'ticketlist', component: TicketListComponent, data: { title: 'Ticket List Page' } },
  { path: 'ticketdetail', component: TicketDetailComponent, data: { title: 'Ticket Detail Page' } },
  { path: 'cart', component: CartComponent, data: { title: 'Cart Page' } },
  { path: 'checkout', component: CheckoutComponent, data: { title: 'Cart Page' } },
  { path: 'payment', component: PaymentComponent, data: { title: 'Cart Page' } },
  { path: 'contactus', component: ContactusComponent, data: { title: 'ContactUs Page' } },
  { path: 'safety', component: SafetyComponent, data: { title: 'Safety Page' } },
  { path: 'whyskyzone', component: WhyskyzoneComponent, data: { title: 'Whyskyzone Page' } },
  // { path: '', component: DefaultLayoutComponent, data: { title: 'Home' },
  //   children: [
  //     { path: 'dashboard', loadChildren: './views/dashboard/dashboard'},
  //     { path: 'ticketlist', loadChildren: './views/ticketlist/ticketlist.component' }
  //   ]
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
   // { useHash: true }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
