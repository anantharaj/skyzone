import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, PathLocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DefaultLayoutComponent } from './layout';
const APP_CONTAINERS = [
  DefaultLayoutComponent
];
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { ModalModule } from "ngx-bootstrap";

import { P404Component } from './p404/p404.component';
import { P500Component } from './p500/p500.component';
import { CallbackComponent } from './callback/callback.component';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { TicketDetailComponent } from './views/ticket-detail/ticket-detail.component';
import { TicketListComponent } from './views/ticket-list/ticket-list.component';
import { CartComponent } from './views/cart/cart.component';
import { CheckoutComponent } from './views/checkout/checkout.component';
import { PaymentComponent } from './views/payment/payment.component';
import { Commonservice } from './services/commonservice';
import { ContactusComponent } from './views/contactus/contactus.component';
import { SafetyComponent } from './views/safety/safety.component';
import { WhyskyzoneComponent } from './views/whyskyzone/whyskyzone.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    CallbackComponent,
    TicketDetailComponent,
    TicketListComponent,
    CartComponent,
    CheckoutComponent,
    PaymentComponent,
    ContactusComponent,
    SafetyComponent,
    WhyskyzoneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule,
    LoadingBarHttpClientModule,
    LoadingBarModule,
    ModalModule.forRoot()
  ],
  providers: [DatePipe, Commonservice, {
    provide: LocationStrategy,
   // useClass: HashLocationStrategy
    useClass: PathLocationStrategy
  }],
  exports: [
    // ModalModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
